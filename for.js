// exercice 1 : créer une fonction qui prend en paramètre un nombre entier et qui
// affiche tous les entiers entre 0 et lui même
console.log('[ARRAY]Exo1 : ', '');
function showNumber(n) {
  const tab = [];
  for (let i = 0; i<=n; i++) {
    tab.push(i);
  }
  return tab;
}
console.log(showNumber(9));
// exercice 2 : créer une fonction qui prend en paramètre 2 nombres entiers et qui
// affiche tous les entiers entre le premier et le second
console.log('[ARRAY]Exo2 : ', '');
function createRandomNumber(min, max) {
  return Math.floor(min + Math.random()*(max - min) + 1);
}

function showNumberInter2Numbers(start, end) {
  const tab = [];
  for (let i = start; i < end; i++) {
    tab.push(i);
  }
  return tab;
}
console.log(showNumberInter2Numbers(0, 10));

// exercice 3 : créer une fonction qui génére un tirage de loto avec
// 7 nombres compris entre 1 et 45
console.log('[ARRAY]Exo3 : ', '');
function goloto() {
  const tab = [];
  for (let i = 0; i < 7; i++) {
    tab.push(createRandomNumber(1, 45));
  }
  return tab;
}
console.log(goloto());

// exercice 4 : générer un tableau contenant des nombres pairs consécutifs,
// le premier nombre du tableau doit être 4,
// on doit arreter de remplir le tableau quand il y a 20 nombres pairs dans le tableau
console.log('[ARRAY]Exo4 : ', '');
function enumEvenNumber() {
  const tab = [];
  for (let i = 4; tab.length < 20; i = i + 2) {
    tab.push(i);
  }
  return tab;
}
console.log(enumEvenNumber());

// exercice 5 : générer un array avec 100 objets avec la forme ci dessous, dont les données sont toutes aléatoires

// const person = {firstName: '', lastName: '', age: 0};
// console.log(person);
console.log('[ARRAY]Exo5 : ', '');
const firstName = ['abdou',
  'dra',
  'modou',
  'mamad',
  'mbaye',
  'bouba',
  'baba',
  'dioneta',
  'ndeye',
];
const lastName = ['samake',
  'coly',
  'malang',
  'ba',
  'ndiaye',
  'diop',
  'fall',
  'gomis',
  'mendy',
  'faye',
  'mbow',
  'sylla',
];
function getPropsValue(array) {
  return array[createRandomNumber(0, array.length)];
}
function personList(end) {
  const tab = [];
  let myObject = {};
  for (let i = 0; i < end; i++) {
    myObject = {
      firstName: getPropsValue(firstName),
      lastName: getPropsValue(lastName),
      age: createRandomNumber(20, 30),
    };
    tab.push(myObject);
  }
  return tab;
}
console.log(personList(100));

// bonus : calculer la moyenne des âges de personnes en utilisant un reduce

const allPersonAge = personList(100).reduce((acc, user) => acc = acc + user.age, 0);
const average = (allPersonAge/100).toFixed(2);
console.log(average);

// exercice 6 : Écrire un programme qui affiche les nombres de 1 à 199 avec
// un console log.
// Mais pour les multiples de 3, afficher “Fizz” au lieu du nombre et pour les multiples de 5 afficher “Buzz”.
// Pour les nombres multiples de 3 et 5, afficher “FizzBuzz”.
// INFO Importante : cet exercice est le plus utilisé au monde dans les tests
console.log('[ARRAY]Exo6 : ', '');
function fizzBuzz() {
  let result = '';
  const tab = [];
  for (let i = 1; i < 199; i++, result = '') {
    if (i%3 === 0) {
      result = result + 'FIZZ';
    }
    if (i%5 === 0) {
      result = result + 'BUZZ';
    }
    tab.push(result || i);
  }
  return tab;
}
console.log(fizzBuzz());

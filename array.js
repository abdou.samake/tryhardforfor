
import {getHostel} from './hotels.data';
console.log(getHostel());
// on fait une fonction qui copie la liste des hotels pour pouvoir la réutiliser
// autant qu'on le veut sans la modifier

// exercice 0 : mettre une majuscule à toutes les RoomName

console.log('[ARRAY]Exo0 : ---------------------------');
function capi(name) {
  return name.charAt(0).toUpperCase() + name.slice(1);
}
function capiAllHotel() {
  return getHostel().map((hotel) => {
    hotel.rooms = hotel.rooms
        .map((room) => {
          room.roomName = capi(room.roomName);
          return room;
        });
    return hotel;
  });
}

console.log(capiAllHotel());

// exercice 1 : trier les hotels par nombre
// de chambres (plus grand en 1er) et créer un tableau
// contenant seulement
// le nom des hotels dans leur ordre de tri
console.log('[ARRAY]Exo1 : ', '');
let hostels1 = getHostel();
hostels1 = hostels1.sort((a, b) => b.roomNumbers - a.roomNumbers)
    .reduce((acc, hotel) => acc.concat(hotel.name), []);
console.log(hostels1);
// exercice 2 : faire un tableau avec toutes les chambres de tous les hotels,
// et ne garder que les chambres qui
// ont plus que 3 places ou exactement 3 places et
// les classer par ordre alphabétique selon le non de la chambre

console.log('[ARRAY]Exo2 : ', '');
let hostels2 = getHostel();
hostels2 = hostels2.reduce((acc, hotel) => acc.concat(hotel.rooms), [])
    .filter((room) => room.size >= 3)
    .sort((a, b) => a.roomName < b.roomName ? -1 : +1);
console.log(hostels2);
// exercice 3 : faire un tableau avec toutes les chambres des hotels
// et ne garder que les chambres qui ont plus de 3 places et dont le
// nom de chambre a une taille supérieure à 15 charactères
console.log('[ARRAY]Exo3 : ', '');
let hostels3 = getHostel();
hostels3 = hostels3.reduce((acc, hotel) => acc.concat(hotel.rooms), [])
    .filter((room) => room.size > 3 && room.roomName.length > 15);
console.log(hostels3);
// exercice 4 : enlever de la liste des hotels toutes les chambres qui ont plus
// de 3 places et changer la valeur de roomNumbers pour qu'elle reflete
// le nouveau nombre de chambres

console.log('[ARRAY]Exo4 : ', '');
let hostels4 = getHostel();
hostels4 = hostels4.map((hotel) => {
  hotel.rooms = hotel.rooms
      .filter((room) => room.size <= 3);
  hotel.roomNumbers = hotel.rooms.length;
  return hotel;
});
console.log(hostels4);
// exercice 5  : extraire du tableau hostels l'hotel qui a le nom 'hotel ocean' en le supprimant du tableau,
// et le mettre dans une nouvelle variable
// puis effacer toutes ses chambres et mettre à jour sa valeur room number, puis pusher l'hotel modifié dans hostels
// puis faire un sort par nom d'hotel
// puis donner le nouvel index de l'hotel océan (faire 2 méthodes : avec indexOf et avec un foreach)

console.log('[ARRAY]Exo5 : ', '');
const hostels5 = getHostel();
const index = hostels5.findIndex((hotel) => hotel.name === 'hotel ocean');
const [newHotel] = hostels5.splice(index, 1);
newHotel.rooms = [];
newHotel.roomNumbers = newHotel.rooms.length;
hostels5.push(newHotel);
hostels5.sort((a, b) => a.name < b.name ? -1 : +1);
const newIndex = hostels5.findIndex((hotel) => hotel.name === 'hotel ocean');
let newIndex2;
hostels5.forEach((hotel, index) => {
  if (hotel.name === 'hotel ocean') {
    newIndex2 = index;
  }
});
console.log(index, hostels5, newHotel, newIndex, newIndex2);


// exercice 6 : créer un objet dont les clés sont le nom des hotels et dont la valeur est un booléen qui indique
// si l'hotel a une chambre qui s'appelle 'suite marseillaise'

console.log('[ARRAY]Exo6 : ', '');
const myObject = {};
const hostels6 = getHostel();
hostels6.forEach((hotel) => myObject[hotel.name] = hotel.rooms
    .some((room) => room.roomName === 'suite marseillaise'));
console.log(myObject);
// exercice 7 : faire une fonction qui prend en paramètre un id d'hotel et qui retourne son nom

console.log('[ARRAY]Exo7 : ', '');
const hostels7 = getHostel();
function nameHotelById(hotelId) {
  const hotelToFind = hostels7.find((hotel) => hotel.id === hotelId);
  if (!hotelToFind) {
    return `hotelToFind doest not exist`;
  }
  return hotelToFind.name;
}
console.log(nameHotelById(1));
console.log(nameHotelById(2));
console.log(nameHotelById(3));

// exercice 8 : faire une fonction qui prend en paramètre un id d'hotel et un id de chambre et qui retourne son nom

console.log('[ARRAY]Exo8 : ', '');
const hostels8 = getHostel();
function nameRoomByHotelIdRoomId(hotelId, roomId) {
  const hotelToFind = hostels8.find((hotel) => hotel.id === hotelId);
  if (!hotelToFind) {
    return `hotelToFind doest not exist`;
  }
  const roomToFind = hotelToFind.rooms.find((room) => room.id === roomId);
  if (!roomToFind) {
    return `room does not exist`;
  }
  return roomToFind.roomName;
}
console.log(nameRoomByHotelIdRoomId(1, 2));
console.log(nameRoomByHotelIdRoomId(1, 3));
// exercice 9: faire une fonction qui prend en paramètre la liste des hotels
// et qui vérifie que toutes les chambres des hotels ont bien
// une majuscule a leur nom et qui renvoie un boolean donnant le résultat.
// Puis faire en sorte que la liste des hotels valide bien cette fonction.

console.log('[ARRAY]Exo9 : ', '');
const hostels9 = getHostel();
function allHotelHaveCapi(hotels) {
  return hotels.every((hotel) => hotel.rooms
      .every((room) => room.roomName === capi(room.roomName)));
}
console.log(allHotelHaveCapi(hostels9));
console.log('[ARRAY]Exo9 avec maj: ', '');
console.log(allHotelHaveCapi(capiAllHotel()));
// exercice 10 : faire une fonction qui prend en paramètre la liste des hotels
// et qui renvoie un tableau contenant les id des hotels
// qui ont au moins une chambre avec plus ou exactement 5 places

console.log('[ARRAY]Exo10 : ', '');
const hostels10 = getHostel();
function arrayContainIdHotel(hostels) {
  return hostels.reduce((acc, hotel) => {
    const roomTofind = hotel.rooms
        .some((room) => room.size >= 5);
    return roomTofind ? acc.concat(hotel.id) : acc;
  }, []);
}
console.log(arrayContainIdHotel(hostels10));

// exercice 11 : faire une fonction qui prend en paramètre la liste des hotels
// et qui renvoie dans un tableau toutes les chambres qui ont plus de 3 places
// et qui sont dans un hotel avec piscine
// puis qui ajoute à chaque chambre une propriété "hotelName"
// qui contient le nom de l'hotel à laquelle elle appartient

console.log('[ARRAY]Exo11 : ', '');
const hostels11 = getHostel();
function allRoomInHotelHavePool(hotels) {
  return hotels.reduce((acc, hotel) => acc.concat(hotel.rooms
      .filter((room) => room.size > 3 && hotel.pool === true)
      .map((room) => {
        room.hotelName = hotel.name;
        return room;
      })), []);
}
console.log(allRoomInHotelHavePool(hostels11));
// exercice 12 : faire une fonction qui prend en paramètre la liste des hotel,
// un id d'hotel et une taille et qui renvoie le nom et l'id de toutes
// les chambres de cet hotel qui font exactement la taille indiquée

console.log('[ARRAY]Exo12 : ', '');
const hostels12 = getHostel();
function nameAndIdAllRooms(hotels, hotelId, size) {
  const hotelToFind = hotels.find((hotel) => hotel.id === hotelId);
  if (!hotelToFind) {
    return `hotel doest not exist`;
  }
  return hotelToFind.rooms.filter((room) => room.size === size)
      .map((room) => {
        (room.roomName, room.id);
        return room;
      });
}
console.log(nameAndIdAllRooms(hostels12, 1, 2));
// exercice 13 : faire une fonction qui prend en paramètre la liste des hotels et
// un id d'hotel et qui supprime cet hotel de la liste des hotels

console.log('[ARRAY]Exo13 : ', '');
const hostels13 = getHostel();
function deleteHotelById(hotels, hotelId) {
  const hotelToFind = hotels.find((hotel) => hotel.id === hotelId);
  if (!hotelToFind) {
    return `hotel doest not exist`;
  }
  return hotels.filter((hotel) => hotel.id !== hotelId);
}
console.log(deleteHotelById(hostels13, 1));
// exercice 14 : faire une fonction qui prend en paramètre la liste des hotel et
// un id d'hotel et un id de chambre et qui supprime cet chambre de l'hotel concerné

console.log('[ARRAY]Exo14 : ', '');
const hostels14 = getHostel();
function deleteRoomByidByroomId(hotels, hotelId, roomId) {
  const hotelToFind = hotels.find((hotel) => hotel.id === hotelId);
  if (!hotelToFind) {
    return `hotel doest not exist`;
  }
  hotelToFind.rooms = hotelToFind.rooms.filter((room) => room.id !== roomId);
  hotelToFind.roomNumbers = hotelToFind.rooms.length;
  return hotels;
}
console.log(deleteRoomByidByroomId(hostels14, 1, 2));

// exercice 15 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel et un boolean et qui va changer la valeur de l'attribut "pool"
// dans l'hotel concerné avec le boolean donné

console.log('[ARRAY]Exo15 : ', '');
const hostels15 = getHostel();
function changePoolHotel(hotels, hotelId, pool) {
  const hotelToFind = hotels.find((hotel) => hotel.id === hotelId);
  if (!hotelToFind) {
    return `hotel doest not exist`;
  }
  hotelToFind.pool = pool;
  return hotels;
}
console.log(changePoolHotel(hostels15, 2, true));

// exercice'); 16 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel et une nouvelle chambre (avec son nom et sa taille) qui
// ajoute la nouvelle chambre dans l'hote concerné et lui donne un id qui suit le
// dernier id de l'hotel

console.log('[ARRAY]Exo16 : ', '');
const hostels16 = getHostel();
function addNewRoom(hotels, hotelId, newRoom) {
  const hotelToFind = hotels.find((hotel) => hotel.id === hotelId);
  if (!hotelToFind) {
    return `hotel doest not exist`;
  }
  hotelToFind.rooms.push(newRoom);
  hotelToFind.roomNumbers = hotelToFind.rooms.length;
  return hotels;
}
console.log(addNewRoom(hostels16, 1, {roomName: 'suite des roses', size: 5}));
// exercice 17 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel, un id de chambre et un nouveau nom de chambre qui va changer le nom
// de la chambre indiquée par le nouveau nom

console.log('[ARRAY]Exo17 : ', '');
const hostels17 = getHostel();
function changeRoomNameById(hotels, hotelId, roomId, newName) {
  const hotelToFind = hotels.find((hotel) => hotel.id === hotelId);
  if (!hotelToFind) {
    return `hotel doest not exist`;
  }
  const roomToFind = hotelToFind.rooms.find((room) => room.id === roomId);
  if (!roomToFind) {
    return `room doest not exist`;
  }
  roomToFind.roomName = newName;
  return hotels;
}
console.log(changeRoomNameById(hostels17, 1, 1, 'suite ramadan'));
